<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>opimonitoring</title>
   <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
   <header>

   </header>
   <nav>
      <ul>
         <li id="header">
            opimonitoring
         </li>
         <li><a href="index.php">Zdjęcia</a></li>
         <li><a href="video.php">Wideo</a></li>
      </ul>
   </nav>
   <main>
      <section>

         <?php
         $maindir = "Monitoring/videos0/";
         $mydir = opendir($maindir);
         $limit = 50;
         $offset = ((int) $_GET['offset']) ? $_GET['offset'] : 0;
         $files = array();
         $page = '';
         $exclude = array(".", "..", "index.php", ".htaccess", "videos0");
         while ($fn = readdir($mydir)) {
            if (!in_array($fn, $exclude)) {
               $files[] = $fn;;
            }
         }
         closedir($mydir);
         sort($files);
         $newICounter = (($offset + $limit) <= sizeof($files)) ? ($offset + $limit) : sizeof($files);

         for ($i = $offset; $i < $newICounter; $i++) {
            ?>
            <video controls>
               <source src="Monitoring/videos0/<?php print $files[$i]; ?>" type="video/mp4" />
            </video>
            <?php
            }
            freddyShowNav($offset, $limit, sizeof($files), "");

            function freddyShowNav($offset, $limit, $totalnum, $query)
            {
               global $PHP_SELF;
               if ($totalnum > $limit) {
                  // calculate number of pages needing links 
                  $pages = intval($totalnum / $limit);

                  // $pages now contains int of pages needed unless there is a remainder from division 
                  if ($totalnum % $limit) $pages++;

                  if (($offset + $limit) > $totalnum) {
                     $lastnum = $totalnum;
                  } else {
                     $lastnum = ($offset + $limit);
                  }
                  ?>

      </section>
      <div id="pagination">
         <div>Strona </div>
         <?php
               for ($i = 1; $i <= $pages; $i++) {  // loop thru 
                  $newoffset = $limit * ($i - 1);
                  if ($newoffset != $offset) {
                     ?>
               <a href="<?php print  $PHP_SELF; ?>?offset=<?php print $newoffset; ?><?php print $query; ?>"><?php print $i; ?>
               </a>
            <?php
                     } else {
                        ?>
               <div class="active"><?php print $i; ?></div>
         <?php
                  }
               }
               ?>

      </div>
<?php
   }
   return;
}

echo $page;
?>
   </main>
   <footer>
      <p>opimonitoring &copy; Wszelkie prawa zastrzeżone <span id="year"></span></p>
   </footer>
   <script src="https://cdn.jsdelivr.net/npm/macy@2.5.0/dist/macy.min.js"></script>
   <script src="js/script.js"></script>
</body>

</html>