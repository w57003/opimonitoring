var year = new Date();
document.querySelector("#year").textContent = year.getFullYear();

var marcyIstance = Macy({
    container: "section",
    mobileFirst: true,
    columns: 1,
    margin:{
        x: 20,
        y: 30,
    },
    breakAt: {
        1024:{
            columns:4
        },
        760: {
          columns: 3
        },
        480: {
            columns: 2
          },
        
    }
})